/*
 * Copyright (C) 2016 Lokiy(liulongke@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  　　　　http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.lokiy.adapter

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

/**
 * Empty Decoration
 */
class EmptyItemDecoration(private val binding: ViewBinding) : RecyclerView.ItemDecoration() {

    private var isMeasure = false

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val emptyView = binding.root
        if (isMeasure.not()) {
            val width = View.MeasureSpec.makeMeasureSpec(parent.width, View.MeasureSpec.EXACTLY)
            val height = View.MeasureSpec.makeMeasureSpec(parent.height, View.MeasureSpec.EXACTLY)
            emptyView.measure(width, height)
            emptyView.layout(0, 0, parent.width, parent.height)
            isMeasure = true
        }
        emptyView.draw(c)
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect[0, 0, parent.width] = parent.height
    }

}