package com.gitee.lokiy

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lokiy.adapter.BindingItem
import com.lokiy.core.adapter.LifecycleViewBindingAdapter
import com.lokiy.adapter.MixViewBindingAdapter
import com.lokiy.adapter.ViewBindingAdapter
import com.lokiy.core.databinding.ItemFooterBinding
import com.lokiy.core.fragment.BasicFragment
import com.lokiy.demo.databinding.FragmentItem1Binding
import com.lokiy.demo.databinding.FragmentItem2Binding
import com.lokiy.demo.databinding.FragmentItem3Binding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicInteger
import kotlin.random.Random

/**
 *
 * @author Lokiy 2022/5/30 15:01
 */
class TestBFragment : BasicFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return RecyclerView(requireContext()).apply {
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setResult(Activity.RESULT_OK, Intent().apply { putExtras(bundleOf("D" to 1)) })


        val adapter = ViewBindingAdapter<String, FragmentItem1Binding> { holder, item ->
            holder.binding.text.text = item
        }

        val lifecycleAdapter = LifecycleViewBindingAdapter<String, FragmentItem1Binding> { holder, item ->
            holder.binding.text.text = item
            text.observe(holder) {
                holder.binding.text.text = it.toString()
            }
        }

        val adapter1 = MixViewBindingAdapter<BindingItem> { holder, item ->
            val binding = holder.binding
            when (item) {
                is A -> {
                    binding as ItemFooterBinding
                    binding.loadingTips.text = holder.adapterPosition.toString()
                }
                is B -> {
                    binding as FragmentItem1Binding
                    binding.text.text = item.text
                }
                is C -> {
                    binding as FragmentItem2Binding
                    binding.text.text = item.text
                    binding.text1.text = item.text1
                }
                is D -> {
                    binding as FragmentItem3Binding
                    binding.text.text = item.text
                    binding.text1.text = item.text1
                    binding.text2.text = item.time
                }
            }
        }
        view as RecyclerView
        view.adapter = adapter1

        adapter.refresh((0..100000).map { it.toString() })
        lifecycleAdapter.refresh((0..100000).map { it.toString() })

        adapter1.refresh((0..100000).map {
            when (Random.nextInt(10) % 4) {
                0 -> A()
                1 -> B(it.toString())
                2 -> C(it.toString(), Random.nextInt(100).toString())
                3 -> D(
                    (0..300).joinToString("") { if (Random.nextBoolean()) "三" else "1" },
                    "部门",
                    Random.nextDouble().toString().take(Random.nextInt(5) + 5)
                )
                else -> A()
            }
        })
    }

    companion object {

        val text = MutableLiveData<Int>()
        private val atomicInteger = AtomicInteger()

        init {
            GlobalScope.launch {
                while (true) {
                    delay(1000)
                    text.postValue(atomicInteger.incrementAndGet())
                }
            }
        }
    }
}