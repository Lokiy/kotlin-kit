package com.gitee.lokiy

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.lokiy.core.fragment.BasicFragment

/**
 *
 * @author Lokiy 2022/5/30 14:59
 */
class TestFragment : BasicFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return TextView(context).apply {
            text = "TestFragment------"
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        view.setOnClickListener {
            startForResult(TestBFragment::class.java, {}, { i: Int, intent: Intent ->
                Log.e("XXX", "-------------${intent.extras?.get("D")}")
            })
//        }
    }

}