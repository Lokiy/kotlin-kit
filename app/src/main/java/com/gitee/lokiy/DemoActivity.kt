/*
 * Copyright (C) 2016 Lokiy(liulongke@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  　　　　http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.gitee.lokiy

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.lokiy.core.activity.CommonActivity
import com.lokiy.adapter.BindingItem
import com.lokiy.core.databinding.ItemFooterBinding
import com.lokiy.demo.databinding.FragmentItem1Binding
import com.lokiy.demo.databinding.FragmentItem2Binding
import com.lokiy.demo.databinding.FragmentItem3Binding
import java.text.SimpleDateFormat
import java.util.*

/**
 * DemoActivity
 *
 * @author Lokiy
 * 2021-06-09 20:54
 */
class DemoActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val textView = TextView(this@DemoActivity)
        textView.text = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA).format(Calendar.getInstance().time)
        textView.keepScreenOn = true
        addContentView(textView, ViewGroup.LayoutParams(-1, -1))

        CommonActivity.startActivity(this, TestFragment::class.java, null)
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }
}

class A(override val binding: Class<out ViewBinding> = ItemFooterBinding::class.java) : com.lokiy.adapter.BindingItem

class B(val text: String, override val binding: Class<out ViewBinding> = FragmentItem1Binding::class.java) : com.lokiy.adapter.BindingItem

class C(val text: String, val text1: String, override val binding: Class<out ViewBinding> = FragmentItem2Binding::class.java) :
    com.lokiy.adapter.BindingItem

class D(val text: String, val text1: String, val time: String, override val binding: Class<out ViewBinding> = FragmentItem3Binding::class.java) :
    com.lokiy.adapter.BindingItem

