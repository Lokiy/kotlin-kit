package com.lokiy.kit.ktx.fragment

import android.util.TypedValue
import androidx.fragment.app.Fragment

/**
 *
 * @author Lokiy
 * @date 2022-06-02 下午4:57
 */

/**
 * covert Int to Dp Value with Context of Fragment
 */
context(Fragment)
val Int.dp
    get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, toFloat(), resources.displayMetrics)
