/*
 * Copyright (C) 2016 Lokiy(liulongke@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  　　　　http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.lokiy.kit.ktx

import android.text.Html

fun String.fromHtml(): CharSequence = Html.fromHtml(this)

/**
 * 如果 等于null 或者 等于空字符串 就返回 def
 */
infix fun String?.ifNullOrEmpty(def: String) = if (isNullOrEmpty()) def else this
