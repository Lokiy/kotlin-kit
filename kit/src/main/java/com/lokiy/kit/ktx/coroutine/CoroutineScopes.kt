/*
 * Copyright (C) 2016 Lokiy(liulongke@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  　　　　http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.lokiy.kit.ktx.coroutine

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataScope
import androidx.lifecycle.liveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * Coroutines
 * @author Lokiy
 * 2021-05-07 15:18
 */


inline fun <T> CoroutineScope.launchAsLiveData(
    context: CoroutineContext = EmptyCoroutineContext,
    crossinline onError: (Throwable) -> String? = {
        it.printStackTrace()
        ""
    },
    crossinline block: suspend LiveDataScope<T>.() -> LiveData<T>
): LiveData<T> = liveData(context + CoroutineExceptionHandler { _, throwable ->
    kotlin.runCatching { onError.invoke(throwable) }.onFailure { it.printStackTrace() }
}) {
    block()
}

inline fun CoroutineScope.launchSafety(
    context: CoroutineContext = EmptyCoroutineContext,
    crossinline onError: (Throwable) -> String? = {
        it.printStackTrace()
        ""
    },
    crossinline block: suspend CoroutineScope.() -> Unit
) = launch(context + CoroutineExceptionHandler { _, throwable ->
    kotlin.runCatching { onError.invoke(throwable) }.onFailure { it.printStackTrace() }
}) {
    block()
}