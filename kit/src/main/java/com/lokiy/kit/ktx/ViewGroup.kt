/*
 * Copyright (C) 2016 Lokiy(liulongke@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  　　　　http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.lokiy.kit.ktx

import android.view.View
import android.view.ViewGroup

/**
 * for each
 */
fun ViewGroup.forEach(child: (View) -> Unit) {
    forEachIndexed { _, view ->
        child(view)
    }
}

/**
 * for each indexed
 */
fun ViewGroup.forEachIndexed(child: (Int, View) -> Unit) {
    val childCount = childCount
    for (i in 0 until childCount) {
        child(i, getChildAt(i))
    }
}

/**
 *
 */
fun ViewGroup.getOrNull(index:Int) = if(index < childCount) getChildAt(index) else null

operator fun ViewGroup.get(index:Int): View = getChildAt(index)