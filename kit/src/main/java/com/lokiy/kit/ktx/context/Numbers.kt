package com.lokiy.kit.ktx.context

import android.content.Context
import android.util.TypedValue

/**
 *
 * @author Lokiy
 * @date 2022-06-02 下午4:57
 */
/**
 * covert Int to Dp Value with Context of Context
 * Used in Activity,Service
 */
context(Context)
val Int.dp
    get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, toFloat(), resources.displayMetrics)
