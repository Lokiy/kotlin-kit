package com.lokiy.kit.ktx.view

import android.content.Context
import android.util.TypedValue
import android.view.View
import androidx.fragment.app.Fragment

/**
 *
 * @author Lokiy
 * @date 2022-06-02 下午4:57
 */

/**
 * covert Int to Dp Value with Context of View
 */
context(View)
val Int.dp
    get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, toFloat(), resources.displayMetrics)
