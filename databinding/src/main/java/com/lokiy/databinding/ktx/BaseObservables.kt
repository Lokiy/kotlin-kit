/*
 * Copyright (C) 2016 Lokiy(liulongke@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  　　　　http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

@file:Suppress("UNCHECKED_CAST")

package com.lokiy.databinding.ktx

import androidx.databinding.Observable
import androidx.lifecycle.*

/**
 * BaseObservables
 * @author Lokiy
 * 2021-04-27 13:18
 */

fun <T : Observable> T.observe(lifecycleOwner: LifecycleOwner, block: (sender: T) -> Unit) {
    val callback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable, propertyId: Int) {
            block.invoke(sender as T)
        }
    }
    val lifecycleObserver = object : LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        fun onStart() {
            removeOnPropertyChangedCallback(callback)
            addOnPropertyChangedCallback(callback)
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun onStop() {
            removeOnPropertyChangedCallback(callback)
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            lifecycleOwner.lifecycle.removeObserver(this)
        }
    }
    removeOnPropertyChangedCallback(callback)
    addOnPropertyChangedCallback(callback)
    lifecycleOwner.lifecycle.addObserver(lifecycleObserver)
}

inline fun <X : Observable, Y> X.map(
    crossinline map: (X) -> Y,
): LiveData<Y> {
    return ObservableMediatorLiveData(this) { mediatorLiveData: ObservableMediatorLiveData<Y>, observable: Observable ->
        mediatorLiveData.postValue(map.invoke(observable as X))
    }
}

inline fun <X : Observable, Y> X.switchMap(
    crossinline map: (X) -> LiveData<Y>,
): LiveData<Y> {
    var mSource: LiveData<Y>? = null
    val block = func@{ mediatorLiveData: ObservableMediatorLiveData<Y>, sender: Observable ->
        val newLiveData = map.invoke(sender as X)
        if (newLiveData == mSource) {
            return@func
        }
        if (mSource != null) {
            mediatorLiveData.removeSource(mSource!!)
        }
        mSource = newLiveData
        if (mSource != null) {
            mediatorLiveData.addSource(mSource!!) { y -> mediatorLiveData.setValue(y) }
        }
    }
    return ObservableMediatorLiveData(this, block)
}

open class ObservableMediatorLiveData<T>(
    private val observable: Observable,
    private val block: (ObservableMediatorLiveData<T>, Observable) -> Unit
) :
    MediatorLiveData<T>() {

    private val callback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable, propertyId: Int) {
            block.invoke(this@ObservableMediatorLiveData, sender)
        }
    }

    override fun onActive() {
        super.onActive()
        observable.addOnPropertyChangedCallback(callback)
    }

    override fun onInactive() {
        super.onInactive()
        observable.removeOnPropertyChangedCallback(callback)
    }

}