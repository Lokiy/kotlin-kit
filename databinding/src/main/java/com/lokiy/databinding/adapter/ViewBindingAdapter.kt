package com.lokiy.databinding.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.lokiy.core.adapter.BasicAdapter
import java.lang.reflect.ParameterizedType

/**
 * ViewBindingAdapter
 *
 */
abstract class ViewBindingAdapter<T, Binding : ViewBinding> : BasicAdapter<T>(onLoadMoreListener = null) {

    val dataList: List<T>
        get() = data

    final override fun onCreateViewHolder1(parent: ViewGroup, viewType: Int): Holder<T?> {
        return object : Holder<T?>(LayoutInflater.from(parent.context).inflate(viewType, parent, false)) {

            val binding: Binding = ((this@ViewBindingAdapter.javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<*>).getMethod("bind", View::class.java).invoke(null, itemView) as Binding

            override fun bindData(data: T?) {
                data ?: return
                onBindDataToView(data, binding, this)
            }

        }
    }

    abstract fun onBindDataToView(item: T, binding: Binding, holder: Holder<T?>)

}
