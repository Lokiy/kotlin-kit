/*
 * Copyright (C) 2016 Lokiy(liulongke@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  　　　　http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.lokiy.core.vm

import androidx.annotation.MainThread
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


/**
 * BasicListViewModel
 * @author Lokiy
 * 2021-02-23 9:57
 */
abstract class BasicListViewModel : ViewModel() {

    protected val mPageIndex: MutableLiveData<Int> = MutableLiveData<Int>(1)

    @MainThread
    fun refresh() {
        mPageIndex.value = 1
    }

    @MainThread
    fun loadMore() {
        mPageIndex.value = (mPageIndex.value ?: 1) + 1
    }

    fun getCurrentPageIndex() = mPageIndex.value

}