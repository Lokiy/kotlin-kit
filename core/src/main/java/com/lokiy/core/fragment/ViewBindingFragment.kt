package com.lokiy.core.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.lokiy.core.ext.actualType
import com.lokiy.core.ext.inflateMethod

/**
 *
 * @author Lokiy
 * @date 2023/1/8 11:12
 */
open class ViewBindingFragment<T : ViewBinding> : BasicFragment() {

    private var _binding: T? = null
    val binding: T
        get() = _binding ?: throw IllegalAccessError("binding == null")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        @Suppress("UNCHECKED_CAST")
        return (javaClass.actualType.inflateMethod.invoke(this, layoutInflater, container, false) as T).also { _binding = it }.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}