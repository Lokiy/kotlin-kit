/*
 * Copyright (C) 2016 Lokiy(liulongke@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  　　　　http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.lokiy.core.fragment

import android.content.Context
import android.util.Log
import android.view.MotionEvent
import androidx.fragment.app.Fragment
import com.lokiy.core.RequestForResult
import com.lokiy.core.internal.DefaultStore
import com.lokiy.core.internal.IStore

/**
 * BasicFragment
 * @author Lokiy
 * 2021-06-09 16:14
 */
open class BasicFragment : Fragment(), IStore by DefaultStore(), RequestForResult {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        registerRequest()
    }

    /**
     * 是否拦截返回事件
     */
    open fun onBackPressed() = false

    open fun onTouchEvent(event: MotionEvent?) = false

    open fun dispatchTouchEvent(ev: MotionEvent?) = false

}