package com.lokiy.core.ext

import android.view.LayoutInflater
import android.view.ViewGroup
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType

/**
 *
 * @author Lokiy
 * @date 2023-02-17 下午3:45
 */


val Class<*>.actualType
    get() = getActualType(0)

val Class<*>.actualType1
    get() = getActualType(1)

val Class<*>.actualType2
    get() = getActualType(2)

val Class<*>.actualType3
    get() = getActualType(3)

fun Class<*>.getActualType(pos: Int) = (genericSuperclass as ParameterizedType).actualTypeArguments[pos] as Class<*>

val Class<*>.inflateMethod: Method
    get() = getDeclaredMethod("inflate", LayoutInflater::class.java, ViewGroup::class.java, Boolean::class.java)
